<?php /* @var $this Controller */
Yii::app()->clientScript->registerCssFile('css/column2.css');
Yii::app()->clientScript->registerPackage('column2');
?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="sidebar">
        <a class="side-name" for="test-menu">Название
            <span></span>
        </a>
        <ul id="test-menu">
            <li>Главная</li>
            <li>Все коктейли</li>
            <li>Коллекции</li>
            <li>Бокалы</li>
            <li>Компоненты</li>
            <li>Фичи</li>
        </ul>
        <div>&nbsp;</div>
        <a class="side-name active" for="test2-menu">Название
            <span></span>
        </a>
        <ul id="test2-menu" class="active">
            <li>Главная</li>
            <li>Все коктейли</li>
            <li>Коллекции</li>
            <li>Бокалы</li>
            <li>Компоненты</li>
            <li>Фичи</li>
        </ul>
    </div>
    <div class="content">
        <?php echo $content; ?>
    </div>
<?php $this->endContent(); ?>